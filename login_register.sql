-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2024 at 02:54 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login_register`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `full_name` varchar(128) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `password`) VALUES
(6, 'david cruz', 'david123@gmail.com', '$2y$10$4pedTg34S6fmjpjCPXXcqOkGJVc16r9Jmz1iIVr1Raezi6.WaP9i6'),
(7, 'glc glc', 'glc@gmail.com', '$2y$10$QRyuVDNf1YXDoa.qwVCTu.WfEJfJ74Lf9k.R7oI9d8CMW0hPhy4pa'),
(8, 'jv que', 'jv@gmail.com', '$2y$10$.6f7G62RsWkA420gOJWgBucpzk3yg102lgDaNjrm0LH3Pf2lMVcIa'),
(9, 'dj cruz', 'dj@gmail.com', '$2y$10$LA55FeoewIXR.Vcc5opZOOI3Imy97T/Vrz0Z0kxd61TH6Ur0pJy9i'),
(10, 'dj dj', 'djdj@gmail.com', '$2y$10$WBRP6wA7SRqcNTa4I09w2uYtpt/7Wy674YOwQbDSgCeg6CeAl38XW'),
(11, 'david david', 'david1@gmail.com', '$2y$10$T5KUmK2l6TvCW3wfx0dv.e3Xn8j9XNi6l94WUAZk0tNcssUDftKC2'),
(12, 'David Jordan', 'davidjordan@gmail.com', '$2y$10$KeX0RM4ybxv3g19gmTjXe.f9LyUQqCHwm0Wpklfx.f7Kb7/el4gsu'),
(13, 'kuroro lucilfer', 'kuroro@gmail.com', '$2y$10$TMM7ShKlQx.gbHwQJqcCY.XMIK1kE0GEHNBFRS3FadeoJizGzhrB2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
